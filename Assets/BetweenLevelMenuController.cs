﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BetweenLevelMenuController : MonoBehaviour
{
    public Button NextLevel, QuitButton, ReplayButton;
    public static int LastLevelIndex;
    public static string LastSceneName;
    private static int _LevelCount = 2;
    void Start()
    {
        //Calls the TaskOnClick/TaskWithParameters/ButtonClicked method when you click the Button
        NextLevel.onClick.AddListener(StartNextLevel);
        QuitButton.onClick.AddListener(QuitGame);
        ReplayButton.onClick.AddListener(ReplayLevel);
        LastLevelIndex = GetLastLevelIndex();
    }

    private int GetLastLevelIndex()
    {
        string levelIndexString = LastSceneName.Substring(5);
        return int.Parse(levelIndexString);
    }

    void StartNextLevel()
    {
        int levelIndexToStart = LastLevelIndex + 1;
        StartLevel(levelIndexToStart);
    }
    void QuitGame()
    {
        Application.Quit();
    }

    void ReplayLevel()
    {
        StartLevel(LastLevelIndex);
    }


    void StartLevel(int levelToStartIndex)
    {
        Debug.Log("starting level " + levelToStartIndex);
        string levelToStartName = "Level" + levelToStartIndex;
        SceneManager.LoadScene(levelToStartName);
    }

}
