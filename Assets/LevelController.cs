﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour
{
    Enemy[] _enemies;
    private static int _nextLevelIndex = 1;
    private static string betweenLevelsMenuSceneName = "BetweenLevelMenu";

    private void OnEnable()
    {
        _enemies = FindObjectsOfType<Enemy>();
    }

    // Update is called once per frame
    void Update()
    {
        foreach (Enemy enemy in _enemies)
        {
            _enemies = FindObjectsOfType<Enemy>();
            if (enemy != null)
            {
                return;
            }
        }

        Debug.Log("You killed all enemies");

        BetweenLevelMenuController.LastSceneName = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(betweenLevelsMenuSceneName);
        if (Application.platform == RuntimePlatform.Android)
        {
            AndroidToastMessage.showToastOnUiThread("You killed all enemies ! Level completed");
        }
    }
}
