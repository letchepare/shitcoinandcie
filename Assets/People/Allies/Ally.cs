﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Ally : MonoBehaviour
{
    private Vector3 _initialPosition;
    private bool _allyWasLaunched;
    private float _timeSittingAroud;
    Vector2 pushRight;
    Vector2 pushLeft;
    Vector2 pushTop;
    float speed = 50;


    [SerializeField] private float _launchPower = 500;


    private void Awake()
    {
        _initialPosition = transform.position;
        pushRight = new Vector2(5f, 0f);
        pushLeft = new Vector2(-5f, 0f);
        pushTop = new Vector2(0f, 5f);

    }

    private void Update()
    {
        GetComponent<LineRenderer>().SetPosition(0, transform.position);
        GetComponent<LineRenderer>().SetPosition(1, _initialPosition);


        if (_allyWasLaunched &&
            GetComponent<Rigidbody2D>().velocity.magnitude <= 0.1)
        {
            _timeSittingAroud += Time.deltaTime;
        }


        if (birdIsOutOfScreen())
        {
            string currentSceneName = SceneManager.GetActiveScene().name;
            SceneManager.LoadScene(currentSceneName);
        }

        bool birdIsOutOfScreen()
        {
            return transform.position.y > 10 ||
                transform.position.y < -10 ||
                transform.position.x > 20 ||
                transform.position.x < -20 ||
                _timeSittingAroud > 3;
        }
    }

    private void OnMouseDown()
    {
        if (_allyWasLaunched)
            return;

        GetComponent<SpriteRenderer>().color = Color.red;
        GetComponent<LineRenderer>().enabled = true;

    }

    private void OnMouseUp()
    {
        if (_allyWasLaunched)
            return;

        GetComponent<SpriteRenderer>().color = Color.white;

        Vector2 directionToInitialPosition = _initialPosition - transform.position;

        GetComponent<Rigidbody2D>().AddForce(directionToInitialPosition * _launchPower);
        GetComponent<Rigidbody2D>().gravityScale = 1;
        _allyWasLaunched = true;
        GetComponent<LineRenderer>().enabled = false;
    }

    private void OnMouseDrag()
    {
        if (_allyWasLaunched)
            return;
        Vector3 newPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector3(newPosition.x, newPosition.y, 0);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        switch (collision.gameObject.tag)
        {
            case "rightWall":
                handleLeftBounce();
                break;
            case "leftWall":
                handleRightBounce();
                break;
        }
    }

    void handleLeftBounce()
    {
        Debug.Log("Ally touched right wall");
        GetComponent<Rigidbody2D>().AddForce(pushLeft * speed);
    }
    void handleRightBounce()
    {
        Debug.Log("Ally touched left wall");
        GetComponent<Rigidbody2D>().AddForce(pushRight * speed);
    }


}
