﻿using UnityEngine;

public class Enemy : MonoBehaviour
{
    Vector2 pushRight;
    Vector2 pushLeft;
    Vector2 pushTop;
    float speed = 20;

    [SerializeField] private GameObject _cloudParticlePrefab;

    private void Awake()
    {
        pushRight = new Vector2(5f, 0f);
        pushLeft = new Vector2(-5f, 0f);
        pushTop = new Vector2(0f, 5f);

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {

        switch (collision.gameObject.tag)
        {
            case "rightWall":
                handleLeftBounce();
                break;
            case "leftWall":
                handleRightBounce();
                break;
        }

        Ally ally = collision.collider.GetComponent<Ally>();

        if (ally != null)
        {
            Instantiate(_cloudParticlePrefab, transform.position, Quaternion.identity);
            Destroy(gameObject);
            return;
        }

        Enemy enemy = collision.collider.GetComponent<Enemy>();

        if (enemy != null)
        {
            return;
        }

        if (collision.contacts[0].normal.y < -0.5)
        {
            Instantiate(_cloudParticlePrefab, transform.position, Quaternion.identity);
            Destroy(gameObject);
            return;
        }
    }

    void handleLeftBounce()
    {
        Debug.Log("Ally touched right wall");
        GetComponent<Rigidbody2D>().AddForce(pushLeft * speed);
    }
    void handleRightBounce()
    {
        Debug.Log("Ally touched left wall");
        GetComponent<Rigidbody2D>().AddForce(pushRight * speed);
    }


}
