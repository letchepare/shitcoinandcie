﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AndroidToastMessage : MonoBehaviour
{

    private static string toastString;
    private static AndroidJavaObject currentActivity;

    public static void showToastOnUiThread(string toastStringToShow)
    {
        AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

        currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        toastString = toastStringToShow;

        currentActivity.Call("runOnUiThread", new AndroidJavaRunnable(showToast));
    }

    public static void showToast()
    {
        Debug.Log("Running on UI thread");
        AndroidJavaObject context = currentActivity.Call<AndroidJavaObject>("getApplicationContext");
        AndroidJavaClass Toast = new AndroidJavaClass("android.widget.Toast");
        AndroidJavaClass Gravity = new AndroidJavaClass("android.view.Gravity");
        AndroidJavaObject javaString = new AndroidJavaObject("java.lang.String", toastString);
        AndroidJavaObject toast = Toast.CallStatic<AndroidJavaObject>("makeText", context, javaString, Toast.GetStatic<int>("LENGTH_SHORT"));
        int position = Gravity.GetStatic<int>("BOTTOM");
        toast.Call("setGravity", position, 0, 0);
        toast.Call("show");
    }
}
